function displayTitle() {
    date = new Date();
    var hours = date.getHours();
    if (hours >= 6) {
        time = 'Доброе утро';
    }
    if ((hours >= 12) && (hours < 18)) {
        time = 'Добрый день';
    }
    if (hours >= 18) {
        time = 'Добрый вечер';
    }
    if ((hours >= 0) && (hours < 6)) {
        time = 'Доброй ночи';
    }
    document.write(time + ', ');
}