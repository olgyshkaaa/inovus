<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>Форма входа</title>

    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">


</head>

<body>

<div >

    <form method="POST" action="${contextPath}/login" class="form-signin">
        <div class="form-group ${error != null ? 'has-error' : ''}">
            <span>${message}</span> <br>
            <span>${error}</span><br>
            <br>
            <span class="data">Имя пользователя:</span>
           <input name="username" type="text" class="form-control"
                   autofocus="true"/>
            <span class="text-center"><a href="${contextPath}/registration">Регистрация</a></span><br>
            <span class="data">Пароль:</span><input name="password" type="password" class="form-control password" >
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <br>
            <button class="btn" type="submit">Войти</button>



        </div>

    </form>

</div>
</body>
</html>
