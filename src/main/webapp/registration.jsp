<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Форма регистрации</title>

    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">


</head>

<body >

<div class="registration">

    <form:form method="POST" modelAttribute="userForm" class="form-signin">
        <spring:bind path="username">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:errors path="username"></form:errors> <br>
                <form:errors path="password"></form:errors> <br>
                <form:errors path="passwordConfirm"></form:errors> <br>
                <span class="data">Имя пользователя:</span>
                <form:input type="text" path="username" class="form-control"
                            autofocus="true"></form:input>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <span class="data">Пароль:</span>
                <form:input type="password" path="password" class="form-control password registration" ></form:input>

            </div>
        </spring:bind>
        <spring:bind path="passwordConfirm">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <span class="data">Повтор пароля:</span>
                <form:input type="password" path="passwordConfirm" class="form-control password confirmation"></form:input>
            </div>
        </spring:bind>


        <button class="btn registration" type="submit">Зарегистрироваться</button>
    </form:form>

</div>

</body>
</html>
