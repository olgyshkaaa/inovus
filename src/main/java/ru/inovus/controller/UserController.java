package ru.inovus.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import ru.inovus.model.User;
import ru.inovus.service.SecurityService;
import ru.inovus.service.UserService;
import ru.inovus.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@Controller
@SessionAttributes(types = User.class)
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        try {
            model.addAttribute("userForm", new User());
            return "registration";
        } catch (Exception e) {
            return "error";
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        try {
            userValidator.validate(userForm, bindingResult);

            if (bindingResult.hasErrors()) {
                return "registration";
            }

            userService.save(userForm);

            securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

            return "redirect:/welcome";
        } catch (Exception e) {
            return "error";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout, SessionStatus status) {
        try {
            if (error != null)
                model.addAttribute("error", "Имя пользователя и/или пароль не подходят");

            if (logout != null) {
                model.addAttribute("message", "Вы вышли из приложения.");
                status.setComplete();
            }

            return "login";
        } catch (Exception e) {
            return "error";
        }
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        try {
            return "welcome";

        } catch (Exception e) {
            return "error";
        }
    }
}
