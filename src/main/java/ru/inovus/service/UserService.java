package ru.inovus.service;

import ru.inovus.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
